import os

import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"


def test_docker_installed(host):
    docker = host.package("docker-ce")

    assert docker.is_installed


@pytest.mark.parametrize("name", [
    "python-pip",
    "python-virtualenv",
    "python-setuptools"
])
def test_python_tools_installed(host, name):
    pytools = host.package(name)

    assert pytools.is_installed


def test_docker_service_enabled(host):
    dockerd = host.service("docker")

    assert dockerd.is_running
    assert dockerd.is_enabled


def test_docker_group(host):
    docker_group = host.group("docker")

    assert docker_group.exists


def test_dockerfile_directory(host):
    dockerfile_dir = host.file("/srv/dockerfiles/")

    assert dockerfile_dir.is_directory
    assert dockerfile_dir.group == "docker"
    assert dockerfile_dir.mode == 0o2770


def test_docker_service_user(host):
    docker_user = host.user("svc_dl-devdocs")

    assert docker_user.home == "/srv/dockerfiles/"
    assert "docker" in docker_user.groups


def test_start_script(host):
    with host.sudo():
        start_script = host.file("/srv/dockerfiles/dl-devdocs_start")

        assert start_script.exists
        assert start_script.user == "svc_dl-devdocs"
        assert start_script.group == "docker"
        assert start_script.mode == 0o0770


def test_unit_file(host):
    unit_file = host.file("/etc/systemd/system/dl-devdocs_container.service")

    assert unit_file.exists
    assert unit_file.user == "root"
    assert unit_file.group == "root"
    assert unit_file.mode == 0o0644


def test_daemonized_container(host):
    containerd = host.service("dl-devdocs_container.service")

    assert containerd.is_running
    assert containerd.is_enabled
