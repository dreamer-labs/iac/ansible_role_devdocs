# Ansible Role DevDocs

This role will configure a Docker container with an Ubuntu 18.04 LTS VM containing a webserver. The webserver hosts a mirror of the contents located at [devdocs website](https://devdocs.io).

## Requirements

Ansible 2.8
Molecule (for testing)

## Role Variables

### Docker Installation Variables

* `devdocs_docker_edition` (default: 'ce') - Edition can be one of 'ce' (Community
  Edition) or 'ee' (Enterprise Edition).

* `devdocs_docker_package` (default: "docker-{{ docker_edition }}") - Provides name of
  docker package to be installed.  Specific version can be specified here as well.

* `devdocs_docker_package_state` (default: present) - Set to `present` to install, and
  `absent` to remove.

* `devdocs_docker_service_state` (default: started) - Set to preferred state of the
  `docker.service` module upon script completion.

* `devdocs_docker_service_enabled` (default: true) - Set to `true` for `docker` to start
  with the system, set to `false` to prevent `docker` from starting with the system.

* `devdocs_docker_restart_handler_state` (default: restarted)

* `devdocs_docker_install_compose` (default: true) - Set to `true` to also install
  `docker-compose`.

* `devdocs_docker_compose_version` (default: "1.22.0") - Set default version of
  `docker-compose` to install.  Only necessary if `docker_install_compose` is set to
  `true`.

* `devdocs_docker_compose_path` (default: /usr/local/bin/docker-compose) - Set path for
  `docker-compose` to be installed to.

* `devdocs_docker_apt_release_channel` (default: stable) - Set which channel
  (`stable`/`testing`) of Docker to pull packages from.

* `devdocs_docker_apt_arch` (default: amd64) - Set architecture of package to pull.

* `devdocs_docker_apt_repository` - Provide string builder for `apt` PPA to be inserted
  into `/etc/apt/sources.list`.

  *Example*:
  `deb [arch={{ docker_apt_arch }}] https://download.docker.com/linux/{{ ansible_distribution|lower }} {{ ansible_distribution_release }} {{ devdocs_docker_apt_release_channel }}`

* `devdocs_docker_apt_ignore_key_error` (default: true) - Choose to have `apt` ignore or
  show any key type errors.

* `devdocs_docker_yum_repo_url` - Provide string builder for `yum`/`dnf` to add to repo
  files in `/etc/yum.repos.d/`.

  *Example*:
  `https://download.docker.com/linux{{ (ansible_distribution == "Fedora") | ternary("fedora","centos") }}/docker-{{ devdocs_docker_edition }}.repo`

* `devdocs_docker_yum_repo_edge` (default: 0) - Set to `1` to pull installation packages
  from the `edge` channel of Docker's official RPM repositories.

* `devdocs_docker_yum_repo_test` (default: 0) - Set to `1` to pull installation packages
  from the `test` channel of Docker's official RPM repositories.

* `devdocs_docker_user` (default: []) - List of users to add to the `docker` group.

### Pull Container Variables

* `devdocs_container_name` (default: dl-devdocs) - Name of the container as it exists in
  a remote container registry.

* `devdocs_container_registry` (default: registry.gitlab.com/nuk380y) - URI for remote
  container registry.

* `devdocs_container_tag` (default: master) - Specific tag of the container.

* `devdocs_container_image` (default: {{ devdocs_container_name }}:{{ devdocs_container_tag }}) -
  Used to access a specific version of the target container.

### SystemD Service Variables

* `devdocs_conatiner_labels` (default: []) - List of `-l` arguments.

* `devdocs_container_host_network` (default: false) - Whether the host network should be
  used.

* `devdocs_container_links` (default: []) - List of `--link` arguments.

* `devdocs_container_ports` (default: []) - List of `-p` arguments.

* `devdocs_container_volumes` (default: []) - List of `-p` arguments.

* `devdocs_container_cap_add` (default: []) - List of capabilities to add.

* `devdocs_container_cap_drop` (default: []) - List of capabilities to drop.

* `devdocs_docker_path` (default: /usr/bin/docker) - Full path to the default location of
  the Docker binary.

* `devdocs_service_enabled` (default: true) - Whether the service should be enabled.

* `devdocs_service_masked` (default: false) - Whether the service should be masked.

* `devdocs_service_state` (default: started) - State the service should be in - set to
  `absent` to remove the service.

* `devdocs_template_env_path` (default: env.j2) - Template file to use for creating the
  environment file.

* `devdocs_template_unit_path` (default: unit.j2) - Template file to use for creating
  service file.

## Dependencies

TBD

## Example Playbook

TBD

## License

See "LICENSE" file.

## Author Information

Dreamer Labs
