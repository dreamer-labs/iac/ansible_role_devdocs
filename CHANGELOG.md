# [2.0.0](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/compare/v1.3.0...v2.0.0) (2019-10-30)


### Features

* Add working method for creating daemonized container ([ca5b9c7](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/commit/ca5b9c7))
* Integrates ansible-docker-systemd-service role by mhutter ([78b9eed](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/commit/78b9eed))


### BREAKING CHANGES

* Added reliable method for managing pulled
container via `systemctl` commands.

# [1.3.0](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/compare/v1.2.0...v1.3.0) (2019-10-18)


### Features

* Add tasks to pull container image ([c79ccb3](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/commit/c79ccb3))

# [1.2.0](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/compare/v1.1.0...v1.2.0) (2019-09-26)


### Features

* Add service user to manage devdocs container ([3dde31d](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/commit/3dde31d))

# [1.1.0](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/compare/v1.0.0...v1.1.0) (2019-09-25)


### Bug Fixes

* Add corrected conditions that allow role to run ([ed07b83](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/commit/ed07b83))
* Correct typos, idempotency, and pycache cleanup for RHEL ([ddda574](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/commit/ddda574))


### Features

* Add files from geerlingguy's Ansible role for Docker ([8c1a50b](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/commit/8c1a50b))

# 1.0.0 (2019-09-17)


### Features

* Bootstrap role ([48d06e6](https://gitlab.com/dreamer-labs/dev-enclave/ansible_role_devdocs/commit/48d06e6))
